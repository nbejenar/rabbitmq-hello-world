package org.springframework.amqp.tutorials.rabbitmqamqptutorials.tut1;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile({"tut1","hello-world"})
@Configuration
public class Tut1Config {

    @Bean
    public Queue hello() {
        return new Queue("hello");
    }

    @Profile("sender")
    @Bean
    public Tut1Sender tut1Sender(){
        return new Tut1Sender();
    }

    @Profile("receiver")
    @Bean
    public Tut1Receiver tut1Receiver(){
        return new Tut1Receiver();
    }
}